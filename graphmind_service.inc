<?php


/**
 * Returns all the available views for GraphMind.
 * @return array
 */
function graphmind_service_get_views() {
  $data = array();

  $tags = variable_get('graphmind_views_tags', '');
  $tags = str_replace(' ', '', $tags);
  $tag_array = explode(',', $tags);

  $views = views_get_all_views();
  foreach ((array)$views as $view) {

    $views_tags = str_replace(' ', '', $view->tag);
    $views_tag_array = explode(',', $views_tags);
    $tag_is_match = strlen($tags) == 0;

    foreach ((array)$views_tag_array as $view_tag) {
      if (in_array($view_tag, $tag_array)) {
        $tag_is_match = TRUE;
      }
    }

    if (!$tag_is_match) continue;

    $data[] = array(
      'name' => $view->name,
      'baseTable' => $view->base_table,
      'tag' => $view->tag,
      'disabled' => $view->disabled,
    );
  }

  return $data;
}


/**
 * Save a GraphMind map. (FreeMind XML format.)
 * @param int $nid
 * @param string $mm
 * @return string
 */
function graphmind_service_save_graphmind($nid, $mm) {
  $node = node_load($nid);
  $node->body = $mm;
  node_save($node);
  return '1';
}
